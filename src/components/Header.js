import React from 'react';

import styles from '../css/components/Header.module.css';

export default ({ children }) => (
  <header className={styles.header}>
    <div className={styles.logo}>
      <h1 className="ta-center">Wet My Leaf</h1>
    </div>
  </header>
);
