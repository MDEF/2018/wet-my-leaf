import React from 'react';

import styles from '../css/components/Footer.module.css';

export default ({ children }) => (
  <footer className={styles.footer}>
    <p>MDEF 2018</p>
  </footer>
);
